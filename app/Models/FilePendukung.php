<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilePendukung extends Model
{
    use HasFactory;
    
    protected $table = 'file_pendukung';
    
    protected $fillable = [
        'nama',
        'jenis',
        'file',
        'url',
        'is_active'
    ];
}
