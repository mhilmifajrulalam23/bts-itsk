<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kegiatan;
use App\Models\User;
use App\Models\provinsi;
use App\Models\laporan;
use App\Models\file_laporan;
use App\Models\FilePendukung;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use RealRashid\SweetAlert\Facades\Alert;
use setasign\Fpdi\Fpdi;


class KontenController extends Controller
{
    public function kegiatan_view()
    {
        $userId = auth()->id();

        $data = Kegiatan::whereHas('users', function ($query) use ($userId) {
            $query->where('id_user', $userId);
        })
            ->with(['users', 'provinsi'])
            ->orderByDesc('created_at')
            ->get();

        return view('user.kegiatan', compact('data'));
    }
    //add kegiatan dari user
    function kegiatanUser_Add_View()
    {
        $usr = User::all();
        $prv = provinsi::all();
        return view('formpengajuan', compact('usr', 'prv'));
    }

    public function KegiatanUser_Store(Request $request)
    {
        try {
            $request->validate([
                'id_provinsi' => 'required|exists:provinsis,id',
                'tanggal_kegiatan' => 'required|date',
                'sekolah' => 'required',
            ]);

            $user = auth()->user();
            $kegiatan = Kegiatan::create([
                'id_provinsi' => $request->id_provinsi,
                'tanggal_kegiatan' => $request->tanggal_kegiatan,
                'sekolah' => strtoupper($request->sekolah),
                'status_promosi' => 'Diproses',
            ]);

            $kegiatan->users()->attach($user->id, ['jabatan' => 'Ketua']);

            // Menambahkan dosen dengan prodi yang sama ke dalam kegiatan
            $dosen = User::where('role', 'dosen')->where('prodi', $user->prodi)->get();
            foreach ($dosen as $dsn) {
                $kegiatan->users()->attach($dsn->id, ['jabatan' => 'Dosen']);
            }
            if ($request->has('id_user')) {
                foreach ($request->id_user as $anggota_id) {
                    if ($anggota_id !== $user->id) {
                        $kegiatan->users()->attach($anggota_id, ['jabatan' => 'Anggota']);
                    }
                }
            }

            Alert::success('Sukses', 'Tambah Kegiatan berhasil')->persistent(true)->autoClose(5000);

            return redirect()->route('view');
        } catch (ValidationException $e) {
            Alert::error('Oops.., Sepertinya ada Kesalahan di Form', $e->validator->errors()->first())->persistent(true)->autoClose(5000);
            return redirect()->back()->withErrors($e->validator)->withInput();
        }
    }

    public function detailKegiatan($id)
    {
        $kegiatan = Kegiatan::findOrFail($id);
        $laporans = Laporan::where('id_kegiatan', $id)->get();
        $filePendukung = FilePendukung::where('is_active', true)->get();
        $laporanWithFiles = Laporan::where('id_kegiatan', $id)->with('files')->whereHas('files')->first();
        $files = $laporanWithFiles ? $laporanWithFiles->files : collect();

        $laporanDiterima = $laporans->firstWhere('status_promosi', 'Diterima');
        $laporanBelumUpload = $laporans->isEmpty();
        return view('user.detail', compact('kegiatan', 'laporans', 'filePendukung', 'files', 'laporanDiterima', 'laporanBelumUpload'));
    }


    public function tambah_laporan($id_kegiatan)
    {
        $kegiatan = Kegiatan::findOrFail($id_kegiatan);
        return view('user.laporan', compact('kegiatan'));
    }

    public function proses_laporan(Request $request)
    {
        $request->validate([
            'id_kegiatan' => 'required|exists:kegiatans,id',
            'file_laporans.*' => 'required|mimes:pdf,docx|max:2048', // Maksimum 2MB per file
        ]);

        $laporan = Laporan::create([
            'id_kegiatan' => $request->id_kegiatan,
            'status_promosi' => 'Diproses',
            'tanggal_laporan' => now(),
            'catatan' => $request->input('catatan', null),
        ]);

        foreach ($request->file('file_laporans') as $file) {
            $filename = time() . '_' . $file->getClientOriginalName();
            $file->storeAs('public/laporans', $filename);

            $file_laporan = file_laporan::create([
                'nama_file' => $filename,
                'dokumen' => 'laporans/' . $filename,
            ]);

            $laporan->files()->attach($file_laporan->id);
        }

        return response()->json(['success' => true, 'message' => 'Laporan berhasil dibuat dengan status Diproses.']);
    }
    public function deleteLaporan($id)
    {
        $laporan = Laporan::find($id);
        if (!$laporan) {
            return redirect()->back()->with('error', 'Laporan tidak ditemukan.');
        }

        foreach ($laporan->files as $file) {
            if (Storage::exists('public/laporans' . $file->dokumen)) {
                Storage::delete('public/laporans' . $file->dokumen);
            }
            $laporan->files()->detach($file->id);

            $file->delete();
        }
        $laporan->delete();
        return redirect()->back()->with('success', 'Laporan dan file-file terkait telah dihapus.');
    }

    public function proses_sertifikat($id)
    {
        $user = auth()->user();
        $kegiatan = Kegiatan::findOrFail($id);

        // Ambil informasi yang diperlukan dari kegiatan
        $nama = $user->nama;
        $skl = $kegiatan->sekolah;

        // Ambil informasi dosen dari pengguna yang terhubung dengan kegiatan melalui pivot
        $dosen = $kegiatan->users()->wherePivot('jabatan', 'Dosen')->first();
        $dsn = $dosen ? $dosen->nama : 'Belum Ada Dosen'; // Ganti 'nama' dengan nama kolom yang sesuai dari model User

        // Tentukan output file untuk sertifikat
        $outputfile = public_path() . 'sertifikat_' . $kegiatan->id . '.pdf';

        // Buat sertifikat dengan informasi yang diperoleh dari kegiatan
        $this->fillPDF(public_path() . '\sertifikat\sertifikat.pdf', $nama, $skl, $dsn, $outputfile);

        // Kembalikan sertifikat sebagai respons file
        return response()->file($outputfile);
    }


    public function fillPDF($file, $nama, $skl, $dsn, $outputfile)
    {
        $fpdi = new Fpdi();
        $fpdi->setSourceFile($file);
        $template = $fpdi->importPage(1);
        $size = $fpdi->getTemplateSize($template);
        $fpdi->addPage($size['orientation'], array($size['width'], $size['height']));
        $fpdi->useTemplate($template);

        // Fungsi untuk menghitung posisi horizontal tengah
        function centerText($fpdi, $text, $y, $fontSize)
        {
            $fpdi->SetFont("Helvetica", "", $fontSize);
            $textWidth = $fpdi->GetStringWidth($text);
            $pageWidth = $fpdi->GetPageWidth();
            $x = ($pageWidth - $textWidth) / 2;
            $fpdi->SetXY($x, $y);
            $fpdi->Write(0, $text);
        }

        // Koordinat vertikal untuk teks
        $topNama = 85;
        $topSkl = 130;
        $topDsn = 185;

        // Set warna teks
        $fpdi->SetTextColor(25, 26, 25);

        // Tambahkan nama di tengah
        centerText($fpdi, $nama, $topNama, 45);

        // Tambahkan sekolah di tengah
        centerText($fpdi, $skl, $topSkl, 30);

        // Tambahkan dosen di tengah
        centerText($fpdi, $dsn, $topDsn, 30);

        return $fpdi->output($outputfile, 'F');
    }
}
