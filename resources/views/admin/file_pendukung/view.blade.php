@extends('admin.dashboard.layouts.main')

@php
    $title = 'File Pendukung';
@endphp

@section('title')
    Dashboard File Pendukung
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 mx-auto mb-5 border overflow-hidden"
        style="background-color: rgb(255, 255, 255); font-size: 13px; margin-top: 125px; border-radius: 10px;">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center p-4">
            <h1 class="fs-5 mb-3 mb-sm-0">Data File Pendukung</h1>
            <a href="{{ route('Admin.file_pendukung.create') }}" class="btn btn-success align-self-end"
                style="border-radius: 25px">
                <span>+</span>
                <span>Tambah Data</span>
            </a>
        </div>
        {{-- <div class="d-flex justify-content-between align-items-center text-secondary px-4">
            <form action="{{ route('Admin.file_pendukung.view') }}" method="get" class="">
                <label for="search">Search :</label>
                <input type="text" name="search" id="search" value="{{ request('search') }}"
                    class="border border-2 px-1" style="width: 200px; border-radius: 5px;">
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div> --}}
        @if (session('success'))
            <div class="mt-3 alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        {{-- Table --}}
        <div class="table-responsive mx-3 mb-3">
            <table class="table table-hover" id="table-file-pendukung">
                <thead class="table-light border-top border-bottom">
                    <tr>
                        <th scope="col" class="text-secondary text-center fw-semibold px-4">NO</th>
                        <th scope="col" class="text-secondary fw-semibold px-4">NAMA FILE</th>
                        <th scope="col" class="text-secondary text-center fw-semibold px-4">JENIS</th>
                        <th scope="col" class="text-secondary text-center fw-semibold px-4">FILE</th>
                        <th scope="col" class="text-secondary text-center fw-semibold px-4">STATUS</th>
                        <th scope="col" class="text-secondary text-center fw-semibold px-4">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($file_pendukung as $key => $file)
                        <tr>
                            <td scope="col" class="text-center px-4">{{ $file_pendukung->firstItem() + $key }}</td>
                            <td scope="col" class="px-4">{{ $file->nama }}</td>
                            <td scope="col" class="text-center px-4">{{ $file->jenis }}</td>
                            <td scope="col" class="text-center px-4">{{ $file->url }}</td>
                            <td scope="col" class="text-center px-4">
                                <form action="{{ route('Admin.file_pendukung.update_status', $file->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-sm toggle-button"
                                        style="background-color: {{ $file->is_active ? '#00ce33' : '#C20C0C' }}; color: white">
                                        {{ $file->is_active ? 'Aktif' : 'Tidak Aktif' }}
                                    </button>
                                </form>
                            </td>
                            <td scope="col" class="text-center px-4">
                                <form action="{{ route('Admin.file_pendukung.delete', $file->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn text-secondary fs-5">
                                        <i class="bi bi-trash3"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- Table End --}}

        {{-- <div class="d-flex justify-content-between text-secondary p-4">
            @if ($file_pendukung instanceof \Illuminate\Pagination\LengthAwarePaginator)
                <div>
                    Showing {{ $file_pendukung->firstItem() }} to {{ $file_pendukung->lastItem() }} of
                    {{ $file_pendukung->total() }} entries
                </div>
            @endif
            <nav>
                <ul class="pagination">
                    @if ($file_pendukung->onFirstPage())
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link text-secondary"
                                href="{{ $file_pendukung->previousPageUrl() }}&search={{ request('search') }}">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @endif

                    @for ($i = 1; $i <= $file_pendukung->lastPage(); $i++)
                        <li class="page-item {{ $i == $file_pendukung->currentPage() ? 'active' : '' }}">
                            <a class="page-link"
                                href="{{ $file_pendukung->url($i) }}&search={{ request('search') }}">{{ $i }}</a>
                        </li>
                    @endfor

                    @if ($file_pendukung->hasMorePages())
                        <li class="page-item">
                            <a class="page-link text-secondary"
                                href="{{ $file_pendukung->nextPageUrl() }}&search={{ request('search') }}">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @else
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div> --}}
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table-file-pendukung').DataTable();
        });
    </script>
@endpush