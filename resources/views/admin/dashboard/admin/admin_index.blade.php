@extends('admin.dashboard.layouts.main')

@php
    $title = 'Admin';
@endphp

@section('title')
    Dashboard Admin
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="box-menu d-flex flex-column align-items-center flex-md-row justify-content-md-evenly mb-5"
        style="margin-top: 125px;">
        <a href="{{ route('dashboard.pesan-masuk') }}"
            class="col-9 col-md-3 d-flex mb-5 mb-md-0 text-dark text-decoration-none shadow-sm">
            <div class="col-4 d-flex justify-content-center py-2" style="background-color: #2F88FF;">
                <i class="bi bi-envelope text-light" style="font-size: 36px;"></i>
            </div>
            <div class="col-8 d-flex align-items-center">
                <p class="mx-3 mb-0">Pesan Masuk<br><span class="fw-bold">{{ $unansweredCount }}</span></p>
            </div>
        </a>

        <a href="{{ route('dash.pendaftar') }}"
            class="col-9 col-md-3 d-flex mb-5 mb-md-0 text-dark text-decoration-none shadow-sm">
            <div class="col-4 d-flex justify-content-center py-2" style="background-color: #19D242;">
                <i class="bi bi-list-ol text-light" style="font-size: 36px;"></i>
            </div>
            <div class="col-8 d-flex align-items-center">
                <p class="mx-3 mb-0">Pendaftar<br><span class="fw-bold">{{ $jumlahDiproses }}</span></p>
            </div>
        </a>

        <a href="{{ route('dash.selesai') }}"
            class="col-9 col-md-3 d-flex mb-5 mb-md-0 text-dark text-decoration-none shadow-sm">
            <div class="col-4 d-flex justify-content-center py-2" style="background-color: #FB8700;">
                <i class="bi bi-clipboard-check text-light" style="font-size: 36px;"></i>
            </div>
            <div class="col-8 d-flex align-items-center">
                <p class="mx-3 mb-0">Telah Selesai<br><span class="fw-bold">{{ $jumlahDiterima }}</span></p>
            </div>
        </a>
    </div>
@endsection