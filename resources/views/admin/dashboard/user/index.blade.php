@extends('admin.dashboard.layouts.main')

@php
    $title = 'User';
@endphp

@section('title')
    Dashboard User
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 mx-auto mb-5 border overflow-hidden"
        style="background-color: rgb(255, 255, 255); font-size: 13px; margin-top: 125px; border-radius: 10px">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center p-4">
            <h1 class="fs-5 mb-3 mb-sm-0">Data User</h1>
            <a href="{{ route('usersmanagement.create') }}" class="btn btn-success align-self-end" style="border-radius: 25px">
                <span>+</span>
                <span>Tambah User</span>
            </a>
        </div>
        {{-- <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center text-secondary px-4">
            <form action="{{ route('admin.usersmanagement.search') }}" method="get" class="">
                @csrf
                <label for="search">Search :</label>
                <input type="text" name="search" id="search" value="{{ request('search') }}"
                    class="border border-2 px-1" style="width: 200px; border-radius: 5px;">
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div> --}}
        @if (session('success'))
            <div class="mt-3 alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif

        {{-- Table --}}
        <div class="table-responsive mx-3 mb-3">
            <table class="table table-hover" id="table-user">
                <thead class="table-light border-top border-bottom">
                    <tr>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">NO</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">NAMA</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">NIM</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">EMAIL</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">ROLE</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">PROGRAM STUDI</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">AKSI</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td class="text-secondary text-center px-3 text-nowrap">{{ $loop->iteration }}</td>
                            <td class="px-3 text-nowrap">{{ $user->nama }}</td>
                            <td class="text-center px-3 text-nowrap">{{ $user->nim }}</td>
                            <td class="px-3 text-nowrap">{{ $user->email }}</td>
                            <td class="px-3 text-nowrap">{{ $user->role }}</td>
                            <td class="px-3 text-nowrap">{{ $user->prodi }}</td>
                            <td class="text-center px-3 text-nowrap">
                                <form action="{{ route('usersmanagement.edit', $user->id) }}" method="get"
                                    class="d-inline">
                                    <button type="submit" name="edit" class="btn text-secondary fs-6 mx-1">
                                        <i class="bi bi-pencil-square"></i> Edit
                                    </button>
                                </form>
                                <form action="{{ route('usersmanagement.destroy', $user->id) }}" method="post"
                                    class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" name="hapus" class="btn text-secondary fs-6 mx-1"
                                        onclick="return confirm('Apakah Anda yakin ingin menghapus user ini?')">
                                        <i class="bi bi-trash3"></i> Hapus
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- Table End --}}

        {{-- <div class="d-flex flex-column flex-md-row justify-content-between align-items-center text-secondary p-4">
            <div>
                Showing {{ $users->firstItem() }} to {{ $users->lastItem() }} of {{ $users->total() }} entries
            </div>
            <nav class="mt-5 mt-md-0">
                <ul class="pagination mb-0">
                    @if ($users->onFirstPage())
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link text-secondary"
                                href="{{ $users->previousPageUrl() }}&search={{ request('search') }}">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @endif

                    @for ($i = 1; $i <= $users->lastPage(); $i++)
                        <li class="page-item {{ $i == $users->currentPage() ? 'active' : '' }}">
                            <a class="page-link"
                                href="{{ $users->url($i) }}&search={{ request('search') }}">{{ $i }}</a>
                        </li>
                    @endfor

                    @if ($users->hasMorePages())
                        <li class="page-item">
                            <a class="page-link text-secondary"
                                href="{{ $users->nextPageUrl() }}&search={{ request('search') }}">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @else
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div> --}}
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table-user').DataTable();
        });
    </script>
@endpush