<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Laporan</title>
</head>
<body>
    <h1>Buat Laporan</h1>
    @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('laporan.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id_kegiatan" value="{{ $kegiatan->id }}">
        
        <!-- Input file absensi -->
        <div>
            <label for="file_absensi">Upload File Absensi:</label>
            <input type="file" id="file_absensi" name="file_laporans[]" accept=".pdf,.docx" required>
        </div>
        
        <!-- Input file dokumentasi kegiatan -->
        <div>
            <label for="file_dokumentasi">Upload File Dokumentasi Kegiatan:</label>
            <input type="file" id="file_dokumentasi" name="file_laporans[]" accept=".pdf,.docx" required>
        </div>
        
        <button type="submit">Submit</button>
    </form>
</body>
</html>

{{-- --------------------------TAMBAH LAPORAN KEGIATAN --------------------------
<!-- Modal -->
<div class="modal fade" id="tambahLaporan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title mx-auto" id="uploadModalLabel">Laporan</h3>
            </button>
          </div>
          <div class="modal-body m-3">
            <form enctype="multipart/form-data">
              <div class="form-group">
                <label for="absenFile"><h5>Absen</h5></label><br>
                <input type="file" class="form-control-file" id="absenFile" style="display: none;">
                <label class="btn btn-light px-6 btn-outline" for="absenFile"><i data-feather="upload" style="color: gray;"></i> upload absen</label>
              </div>
              <div class="form-group mt-4">
                <label for="dokumentasiFile"><h5>Dokumentasi Kegiatan</h5></label><br>
                <input type="file" class="form-control-file" id="dokumentasiFile" style="display: none;">
                <label class="btn btn-light px-6 btn-outline" for="absenFile"><i data-feather="upload" style="color: gray;"></i> upload dokumentasi</label>
              </div>
            </form>
            <button type="button" class="btn btn-dark mt-4 d-block mx-auto pt-3 pb-3" style="width:85%;">Kirim</button>
          </div>
        </div>
      </div>
  </div> --}}