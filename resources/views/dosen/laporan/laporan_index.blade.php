@extends('dosen.layouts.main')

@php
    $title = 'Laporan';
@endphp

@section('title')
    Dashboard Laporan
@endsection

@section('sidebar_item')
    @include('dosen.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 mx-auto mb-5 border overflow-hidden"
        style="background-color: rgb(255, 255, 255); font-size: 13px; margin-top: 125px; border-radius: 10px">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center p-4">
            <h1 class="fs-5 mb-3 mb-sm-0">Data Laporan</h1>
            {{-- <a href="{{ url('/dashboard-tambah-laporan') }}" class="btn btn-success align-self-end"
                style="border-radius: 25px">
                <span>+</span>
                <span>Tambah Data</span>
            </a> --}}
        </div>
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center text-secondary px-4">
            <form action="#" method="post" class="mb-3 mb-sm-0">
                <label for="show">Show</label>
                <input type="number" name="show" id="show" value="10" class="border border-2"
                    style="width: 75px; border-radius: 5px;">
                <span>entries</span>
            </form>
            <form action="#" method="post" class="">
                <label for="search">Search :</label>
                <input type="text" name="search" id="search" class="border border-2 px-1"
                    style="width: 200px; border-radius: 5px;">
            </form>
        </div>

        {{-- Table --}}
        <div class="table-responsive mt-4">
            <table class="table table-hover">
                <thead class="table-light border-top border-bottom">
                    <tr>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">ID</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">NAMA KETUA</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">NAMA SEKOLAH</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">TANGGAL LAPORAN</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">FILE LAPORAN</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">STATUS</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">ACTION</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($laporans as $laporan)
                        <tr>
                            <td class="text-secondary text-center px-3 text-nowrap">{{ $loop->iteration }}</td>
                            <td class="px-3 text-nowrap">
                                {{ $laporan->kegiatan->users->where('pivot.jabatan', 'Ketua')->first()->nama ?? 'N/A' }}
                            </td>
                            <td class="px-3 text-nowrap">{{ $laporan->kegiatan->sekolah }}</td>
                            {{-- <td class="px-3 text-center text-nowrap">
                            <form action="#" method="post" class="">
                                <button type="submit" name="lihat" class="btn btn-primary"
                                    style="font-size: 13px; border-radius: 25px">
                                    <span style="margin-right: 5px"><i class="bi bi-eye"></i></span>
                                    <span>Lihat</span>
                                </button>
                            </form>
                        </td> --}}
                            <td class="px-3 text-nowrap">{{ $laporan->tanggal_laporan }}</td>

                            <td class="px-3 text-nowrap">
                                @foreach ($laporan->files as $file)
                                    <a href="{{ asset('storage/' . $file->dokumen) }}"
                                        target="_blank">{{ $file->nama_file }}</a><br>
                                @endforeach
                            </td>
                            
                            <td class="px-3 text-center text-nowrap">
                                @if ($laporan->status_promosi == 'Proses')
                                    <span class="badge text-bg-secondary fw-normal pb-2"
                                        style="font-size: 13px">Proses</span>
                                @elseif($laporan->status_promosi == 'Diterima')
                                    <span class="badge text-bg-success fw-normal pb-2"
                                        style="font-size: 13px">Selesai</span>
                                @elseif($laporan->status_promosi == 'Ditolak')
                                    <span class="badge text-bg-danger fw-normal pb-2" style="font-size: 13px">Ditolak</span>
                                @else
                                    <span class="badge text-bg-secondary fw-normal pb-2"
                                        style="font-size: 13px">{{ $laporan->status_promosi }}</span>
                                @endif
                            </td>

                            <td class="text-center px-3 text-nowrap">
                                <!-- Form with button to trigger modal -->
                                <form action="#" method="get" class="d-inline">
                                    <button type="button" name="lihat" class="btn text-secondary fs-5 mx-1"
                                        data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                        <i class="bi bi-eye"></i>
                                    </button>
                                </form>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                ...
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <form action="{{ route('dosen.laporan.edit', $laporan->id) }}" method="get"
                                    class="d-inline">
                                    <button type="submit" name="edit" class="btn  text-secondary fs-5 mx-1">
                                        <i class="bi bi-pencil-square"></i>
                                    </button>
                                </form>
                                {{-- <form action="{{ route('admin.laporan.delete', $laporan->id) }}" method="post"
                                    class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" name="hapus" class="btn text-secondary fs-5 mx-1"
                                        onclick="return confirm('Apakah Anda yakin ingin menghapus kegiatan ini?')">
                                        <i class="bi bi-trash3"></i>
                                    </button>
                                </form> --}}

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- Table End --}}

        <div class="d-flex flex-column flex-md-row justify-content-between align-items-center text-secondary p-4">
            <div>
                Showing {{ $laporans->firstItem() }} to {{ $laporans->lastItem() }} of {{ $laporans->total() }} entries
            </div>
            <nav class="mt-5 mt-md-0">
                <ul class="pagination mb-0">
                    @if ($laporans->onFirstPage())
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link text-secondary" href="{{ $laporans->previousPageUrl() }}">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @endif

                    @for ($i = 1; $i <= $laporans->lastPage(); $i++)
                        <li class="page-item {{ $i == $laporans->currentPage() ? 'active' : '' }}">
                            <a class="page-link" href="{{ $laporans->url($i) }}">{{ $i }}</a>
                        </li>
                    @endfor

                    @if ($laporans->hasMorePages())
                        <li class="page-item">
                            <a class="page-link text-secondary" href="{{ $laporans->nextPageUrl() }}">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @else
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
@endsection

{{-- <div class="container">
    <h1>Daftar Laporan</h1>
    <table class="table">
        <thead>
            <tr>
                <th>ID Laporan</th>
                <th>ID Kegiatan</th>
                <th>Status Promosi</th>
                <th>Tanggal Laporan</th>
                <th>Aksi</th>
                <th>File Laporan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($laporan as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->id_kegiatan }}</td>
                    <td>{{ $item->status_promosi }}</td>
                    <td>{{ $item->tanggal_laporan }}</td>
                    <td>
                        <a href="{{ route('dosen.laporan.edit', $item->id) }}" class="btn btn-primary">Edit</a>
                    </td>
                    <td>
                        @foreach ($item->files as $file)
                            <a href="{{ asset('storage/' . $file->dokumen) }}" target="_blank">{{ $file->nama_file }}</a><br>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div> --}}
