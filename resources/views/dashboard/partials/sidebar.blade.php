<style>
    #admin-sidebar {
        height: 100vh;
        overflow-y: auto;
        overflow-x: hidden;
    }

    /* Menyembunyikan teks pada tampilan mobile */
    @media (max-width: 1200px) {
        .nav-link span {
            display: none;
        }
    }
</style>

<section id="admin-sidebar" class="bg-dark">
    <div class="sidebar">
        <div class="sidebar-logo text-center">
            <!-- Logo -->
            <img src="#" alt="Logo" height="200">
        </div>
        <ul class="nav flex-column px-6">
            <!-- Beranda -->
            <li class="nav-item">
                <a class="nav-link text-white" href="#">
                    <i data-feather="home" class="text-secondary"></i>
                    <span>Beranda</span>
                </a>
            </li>
            <!-- Bahan Presentasi -->
            <li class="nav-item">
                <a class="nav-link text-white" href="#">
                    <i data-feather="file-text" class="text-secondary"></i>
                    <span>Bahan Presentasi</span>
                </a>
            </li>
            <!-- Kegiatan -->
            <li class="nav-item">
                <a class="nav-link text-white" href="#">
                    <i data-feather="calendar" class="text-secondary"></i>
                    <span>Kegiatan</span>
                </a>
            </li>
            <!-- Laporan -->
            <li class="nav-item">
                <a class="nav-link text-white" href="#">
                    <i data-feather="file" class="text-secondary"></i>
                    <span>Laporan</span>
                </a>
            </li>
            <!-- Sertifikat -->
            <li class="nav-item">
                <a class="nav-link text-white" href="#">
                    <i data-feather="award" class="text-secondary"></i>
                    <span>Sertifikat</span>
                </a>
            </li>
        </ul>
    </div>
</section>
