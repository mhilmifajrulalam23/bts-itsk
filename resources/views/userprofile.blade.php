@extends('layouts.main')

@section('title', "BTS-ITSK | Profil")

@section('content')
    {{-- NAVBAR  --}}
    @include('partials.navbar')

    {{-- USER PROFILE  --}}
    @include('partials.userprofile')

    {{-- FOOTER  --}}
    @include('partials.footer')
@endsection